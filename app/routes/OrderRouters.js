const express = require("express");
const { createOrderOfCustomer, getAllOrder, getAllOrderOfCustomer, getOrderById, updateOrder, deleteOrder } = require("../controllers/OrderControllers");
const OrderRouter = express.Router();

OrderRouter.post("/customers/:customerId/orders", createOrderOfCustomer);
OrderRouter.get("/orders", getAllOrder);
OrderRouter.get("/customers/:customerId/orders", getAllOrderOfCustomer);
OrderRouter.get("/orders/:orderId", getOrderById);
OrderRouter.put("/orders/:orderId", updateOrder);
OrderRouter.delete("/customers/:customerId/orders/:orderId", deleteOrder);

module.exports = OrderRouter;