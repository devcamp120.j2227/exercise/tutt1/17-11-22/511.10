const express = require("express");
const { createProduct, getAllProduct, getProductById, updateProduct, deleteProduct } = require("../controllers/ProductControllers");
const ProductRouter = express.Router();

ProductRouter.post("/products/:productTypeId", createProduct);
ProductRouter.get("/products", getAllProduct);
ProductRouter.get("/products/:productId", getProductById);
ProductRouter.put("/productTypes/:productTypeId/products/:productId", updateProduct);
ProductRouter.delete("/products/:productId", deleteProduct);

module.exports = ProductRouter