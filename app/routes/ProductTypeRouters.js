const express = require("express");
const { CreateProductType, GetAllProductType, GetProductTypeById, UpdateProductType, DeleteProductType } = require("../controllers/ProductTypeControlllers");
const ProductTypeRouter = express.Router();

ProductTypeRouter.post("/productTypes", CreateProductType);
ProductTypeRouter.get("/productTypes", GetAllProductType);
ProductTypeRouter.get("/productTypes/:ProductTypeId", GetProductTypeById);
ProductTypeRouter.put("/productTypes/:ProductTypeId", UpdateProductType);
ProductTypeRouter.delete("/productTypes/:ProductTypeId", DeleteProductType);

module.exports = ProductTypeRouter