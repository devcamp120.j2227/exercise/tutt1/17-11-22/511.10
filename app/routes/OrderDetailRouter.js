const express = require("express");
const { createOrderDetailOfOrder, getAllOrderDetail, getAllOrderDetailOfOrder, getOrderDetailById, updateOrderDetail, deleteOrderDetail } = require("../controllers/OrderDetailControllers");
const OrderDetailRouter = express.Router();

OrderDetailRouter.post("/orders/:orderId/orderDetails", createOrderDetailOfOrder);
OrderDetailRouter.get("/orderDetails", getAllOrderDetail);
OrderDetailRouter.get("/orders/:orderId/orderDetails", getAllOrderDetailOfOrder);
OrderDetailRouter.get("/orderDetails/:orderDetailId", getOrderDetailById);
OrderDetailRouter.put("/orderDetails/:orderDetailId", updateOrderDetail);
OrderDetailRouter.delete("/orders/:orderId/orderDetails/:orderDetailId", deleteOrderDetail);

module.exports = OrderDetailRouter;