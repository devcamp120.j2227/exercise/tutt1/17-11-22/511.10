const express = require("express");
const { createCustomer, getAllCustomer, getCustomerById, updateCustomer, deleteCustomer } = require("../controllers/CustomerControllers");
const CustomerRouter = express.Router();

CustomerRouter.post("/customers", createCustomer);
CustomerRouter.get("/customers", getAllCustomer);
CustomerRouter.get("/customers/:customerId", getCustomerById);
CustomerRouter.put("/customers/:customerId", updateCustomer);
CustomerRouter.delete("/customers/:customerId", deleteCustomer);

module.exports = CustomerRouter