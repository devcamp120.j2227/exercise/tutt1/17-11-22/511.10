const mongoose = require("mongoose");
const CustomerModel = require("../models/Customer.js");

const createCustomer = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!body.fullName) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "fullName is required"
        })
    }
    if(!body.phone) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "phone is required"
        })
    }
    if(!body.email) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "email is required"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let newCustomer = {
        _id: mongoose.Types.ObjectId(),
	    fullName: body.fullName,
	    phone: body.phone,
        email: body.email,
	    address: body.address,
        city: body.city,
        country: body.country,
        orders: body.orders
    }
    CustomerModel.create(newCustomer, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully create Customer",
                data: data
            })
        }
    })
}

const getAllCustomer = (req, res) => {
    // B1: Thu thập dữ liệu
    // B2: Kiểm tra dữ liệu
    // B3: Xử lý và hiển thị kết quả
    CustomerModel.find()
    .populate("orders")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get all Customer",
                data: data
            })
        }
    })
}

const getCustomerById = (req, res) => {
    // B1: Thu thập dữ liệu
    let customerId = req.params.customerId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "customerId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    CustomerModel.findById(customerId)
    .populate("orders")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get Customer by Id",
                data: data
            })
        }
    })
}

const updateCustomer = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    let customerId = req.params.customerId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "customerId is invalid"
        })
    }
    if(!body.fullName) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "fullName is required"
        })
    }
    if(!body.phone) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "phone is required"
        })
    }
    if(!body.email) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "email is required"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let updateCustomer = {
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country,
        orders: body.orders
    }
    CustomerModel.findByIdAndUpdate(customerId, updateCustomer)
    .populate("orders")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully update Customer",
                data: data
            })
        }
    })
}

const deleteCustomer = (req, res) => {
    // B1: Thu thập dữ liệu
    let customerId = req.params.customerId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "customerId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    CustomerModel.findByIdAndDelete(customerId)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully delete Customer by Id",
            })
        }
    })
}

module.exports = {
    createCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomer,
    deleteCustomer
}