const mongoose = require("mongoose");
const ProductModel = require("../models/Product.js");

const createProduct = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    let productTypeId = req.params.productTypeId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "productTypeId is invalid"
        })
    }
    if(!body.name) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "name is required"
        })
    }
    if(!body.imageUrl) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "imageUrl is required"
        })
    }
    if(!body.buyPrice || !Number.isInteger(body.buyPrice) || body.buyPrice < 0) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "buyPrice is invalid"
        })
    }
    if(!body.promotionPrice || !Number.isInteger(body.promotionPrice) || body.promotionPrice < 0) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "promotionPrice is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let newProduct = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
        type: productTypeId,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    }
    ProductModel.create(newProduct, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully create Product",
                data: data
            })
        }
    })
}

const getAllProduct = (req, res) => {
    // B1: Thu thập dữ liệu

    // B2: Kiểm tra dữ liệu

    // B3: Xử lý và hiển thị kết quả
    ProductModel.find()
    .populate("type")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get all Product",
                data: data
            })
        }
    })
}

const getProductById = (req, res) => {
    // B1: Thu thâp dữ liệu
    let productId = req.params.productId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "ProductId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    ProductModel.findById(productId)
    .populate("type")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get Product by Id",
                data: data
            })
        }
    })
}

const updateProduct = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    let productId = req.params.productId;
    let productTypeId = req.params.productTypeId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "productTypeId is invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "ProductId is invalid"
        })
    }
    if(!body.name) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "name is required"
        })
    }
    if(!body.imageUrl) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "imageUrl is required"
        })
    }
    if(!body.buyPrice || !Number.isInteger(body.buyPrice) || body.buyPrice < 0) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "buyPrice is invalid"
        })
    }
    if(!body.promotionPrice || !Number.isInteger(body.promotionPrice) || body.promotionPrice < 0) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "promotionPrice is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let updateProduct = {
        name: body.name,
        description: body.description,
        type: productTypeId,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    }
    ProductModel.findByIdAndUpdate(productId, updateProduct)
    .populate("type")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully update Product by Id",
                data: data
            })
        }
    })
}

const deleteProduct = (req, res) => {
    // B1: Thu thâp dữ liệu
    let productId = req.params.productId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "ProductId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    ProductModel.findByIdAndDelete(productId)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully delete Product by Id"
            })
        }
    })
}

module.exports = {
    createProduct,
    getAllProduct,
    getProductById, 
    updateProduct,
    deleteProduct
}