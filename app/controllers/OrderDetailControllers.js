const mongoose = require("mongoose");
const OrderDetailModel = require("../models/OrderDetail");
const OrderModel = require("../models/Order");
const { updateOrder } = require("./OrderControllers");

const createOrderDetailOfOrder = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderId = req.params.orderId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "orderId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let newOrderDetail = {
        _id: mongoose.Types.ObjectId(),
        product: body.product,
        quantity: body.quantity,
    }
    OrderDetailModel.create(newOrderDetail, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        OrderModel.findByIdAndUpdate(orderId, {
            $push: {orderDetails: data._id}
        }, (err, updateCustomer) => {
            if(err) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: err.message
                })
            } else {
                return res.status(201).json({
                    status: "Successfully create orderDetail",
                    data: data
                })
            }
        })
        
    })
}

const getAllOrderDetail = (req, res) => {
    // B1: Thu thập dữ liệu
    // B2: Kiểm tra dữ liệu
    // B3: Xử lý và hiển thị kết quả
    OrderDetailModel.find()
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get all orderDetail",
                data: data
            })
        }
    })
}

const getAllOrderDetailOfOrder = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderId = req.params.orderId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "orderId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    OrderModel.findById(orderId)
    .populate("orderDetails")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get all orderDetails of order",
                data: data
            })
        }
    })
}

const getOrderDetailById  = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderDetailId = req.params.orderDetailId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "orderDetailId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    OrderDetailModel.findById(orderDetailId)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get orderDetail by id",
                data: data
            })
        }
    })
}

const updateOrderDetail = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    let orderDetailId = req.params.orderDetailId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "orderDetailId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let updateOrderDetail = {
        product: body.product,
        quantity: body.quantity,
    }
    OrderDetailModel.findByIdAndUpdate(orderDetailId, updateOrderDetail)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully update orderDetail",
                data: data
            })
        }
    })
}

const deleteOrderDetail = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderDetailId = req.params.orderDetailId;
    let orderId = req.params.orderId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "orderDetailId is invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "orderId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    OrderDetailModel.findByIdAndDelete(orderDetailId)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        OrderModel.findByIdAndUpdate(orderId, {
            $pull: {orderDetails: orderDetailId}
        }, (err) => {
            if(err) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: err.message
                })
            } else {
                return res.status(200).json({
                    status: "Successfully delete orderDetail"
                })
            }
        })
    })
}

module.exports = {
    createOrderDetailOfOrder,
    getAllOrderDetail,
    getAllOrderDetailOfOrder,
    getOrderDetailById,
    updateOrderDetail,
    deleteOrderDetail
}