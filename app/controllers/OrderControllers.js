const mongoose = require("mongoose");
const OrderModel = require("../models/Order");
const CustomerModel = require("../models/Customer");
const { updateCustomer } = require("./CustomerControllers");

const createOrderOfCustomer = (req, res) => {
    // B1: Thu thập dữ liệu
    let customerId = req.params.customerId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "customerId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let newOrder = {
        _id: mongoose.Types.ObjectId(),
        orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        orderDetails: body.orderDetails,
        cost: body.cost
    }
    OrderModel.create(newOrder, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        CustomerModel.findByIdAndUpdate(customerId, {
            $push: {orders: data._id}
        }, (err, updateCustomer) => {
            if(err) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: err.message
                })
            } else {
                return res.status(201).json({
                    status: "Successfully create order",
                    data: data
                })
            }
        })
        
    })
}

const getAllOrder = (req, res) => {
    // B1: Thu thập dữ liệu
    // B2: Kiểm tra dữ liệu
    // B3: Xử lý và hiển thị kết quả
    OrderModel.find()
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get all order",
                data: data
            })
        }
    })
}

const getAllOrderOfCustomer = (req, res) => {
    // B1: Thu thập dữ liệu
    let customerId = req.params.customerId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "customerId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    CustomerModel.findById(customerId)
    .populate("orders")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get all order of customer",
                data: data
            })
        }
    })
}

const getOrderById = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderId = req.params.orderId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "orderId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    OrderModel.findById(orderId)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get order by id",
                data: data
            })
        }
    })
}

const updateOrder = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    let orderId = req.params.orderId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "orderId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let updateOrder = {
        orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        orderDetails: body.orderDetails,
        cost: body.cost
    }
    OrderModel.findByIdAndUpdate(orderId)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully update order",
                data: data
            })
        }
    })
}

const deleteOrder = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderId = req.params.orderId;
    let customerId = req.params.customerId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "orderId is invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "customerId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    OrderModel.findByIdAndDelete(orderId)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        CustomerModel.findByIdAndUpdate(customerId, {
            $pull: {orders: orderId}
        }, (err) => {
            if(err) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: err.message
                })
            } else {
                return res.status(200).json({
                    status: "Successfully delete order"
                })
            }
        })
    })
}

module.exports = {
    createOrderOfCustomer,
    getAllOrder,
    getAllOrderOfCustomer,
    getOrderById,
    updateOrder,
    deleteOrder
}