const mongoose = require("mongoose");
const ProductTypeModel = require("../models/ProductType");

const CreateProductType = (req, res) => {
    //B1: Thu thập dữ liệu
    let body = req.body;
    //B2: Kiểm tra dữ liệu
    if(!body.name) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "Name is not required"
        })
    }
    //B3: Xử lý và hiển thị kết quả
    let newProductType = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description
    }
    ProductTypeModel.create(newProductType, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully create ProductType",
                data: data
            })
        }
    })
}

const GetAllProductType = (req, res) => {
    // B1:  Thu thập dữ liệu
    // B2: Kiểm tra dữ liệu
    // B3: Xử lý và hiển thị kết quả
    ProductTypeModel.find((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get all ProductType",
                data: data
            })
        }
    })
}

const GetProductTypeById = (req, res) => {
    // B1: Thu thập dữ liệu
    let ProductTypeId = req.params.ProductTypeId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(ProductTypeId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "ProductTypeId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    ProductTypeModel.findById(ProductTypeId, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get ProductType by Id",
                data: data
            })
        }
    })
}

const UpdateProductType = (req, res) => {
    // B1: Thu Thập dữ liệu
    let body = req.body;
    let ProductTypeId = req.params.ProductTypeId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(ProductTypeId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "ProductTypeId is invalid"
        })
    }
    if(!body.name) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "Name is not required"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let newProductType = {
        name: body.name,
        description: body.description
    }
    ProductTypeModel.findByIdAndUpdate(ProductTypeId, newProductType, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully update ProductType",
                data: data
            })
        }
    })
}

const DeleteProductType = (req, res) => {
    // B1: Thu Thập dữ liệu
    let ProductTypeId = req.params.ProductTypeId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(ProductTypeId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "ProductTypeId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    ProductTypeModel.findByIdAndDelete(ProductTypeId, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully Delete ProductType",
            })
        }
    })
}

module.exports = { CreateProductType, GetAllProductType, GetProductTypeById, UpdateProductType, DeleteProductType }