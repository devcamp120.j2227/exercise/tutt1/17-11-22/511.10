const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ProductTypeSchema = new Schema({
    name: {
        type: String,
        unique: true,
        require: true
    },
    description: {
        type: String
    }
},{
    timestamps: true
})

module.exports = mongoose.model("ProductType", ProductTypeSchema);