const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const OrderDetailSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    product: {
        type: mongoose.Types.ObjectId,
        ref: "Product"
    },
    quantity: {
        type: Number,
        default: 0
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("OrderDetail", OrderDetailSchema);