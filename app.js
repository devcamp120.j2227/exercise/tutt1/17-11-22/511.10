const express = require("express");
const mongoose = require("mongoose");
const CustomerRouter = require("./app/routes/Customers");
const OrderDetailRouter = require("./app/routes/OrderDetailRouter");
const OrderRouter = require("./app/routes/OrderRouters");
const ProductRouter = require("./app/routes/ProductRouters");
const ProductTypeRouter = require("./app/routes/ProductTypeRouters");
const app = express();
app.use(express.json());
const port = 8000;

mongoose.connect("mongodb://localhost:27017/SHOP24H_BACKEND", (error) => {
    if(error) throw error;
    console.log("Successfully connected");
});

app.use("/", ProductTypeRouter);
app.use("/", ProductRouter);
app.use("/", CustomerRouter);
app.use("/", OrderRouter);
app.use("/", OrderDetailRouter);

app.listen(port, () => {
    console.log("App listerning on port", port);
});